import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Redirect, useHistory } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  //allows the use of the UserContext object and its properties for user validation
  const { user } = useContext(UserContext);
  const history = useHistory();

  //state hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobile] = useState("");
  const [age, setAge] = useState("");

  //state to determine whether submit button is enabled or not
  const [isActive, setisActive] = useState(false);

  //check if values are successfully binded
  console.log(email);
  console.log(password1);
  console.log(password2);
  console.log(firstName);
  console.log(lastName);
  console.log(mobileNo);
  console.log(age);

  //QUESTION: how to know whether to use a function or useEffect for the fetch?

  //function to simulate user registration
  //activity: register user
  function registerUser(e) {
    //prevents page redirection via form submission
    e.preventDefault();

    fetch("http://localhost:4000/api/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        //data returns true if the email exists
        if (data === true) {
          Swal.fire({
            title: "Email already exists",
            icon: "error",
            text: "Please try again. ",
          });
        } else {
          fetch("http://localhost:4000/api/users/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              age: age,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data === true) {
                setEmail("");
                setPassword1("");
                setPassword2("");
                setFirstName("");
                setLastName("");
                setMobile("");
                setAge("");
                Swal.fire({
                  title: "Successfully registered",
                  icon: "success",
                  text: "You have successfully registered.",
                });
                history.push("/login");
              } else {
                Swal.fire({
                  title: "Something wrong",
                  icon: "error",
                  text: "Please try again.",
                });
              }
            });
        }
      });

    //clear input once submitted

    //alert("Thank you for registering!");
  }

  // putting mobileNo number into an array to check length
  const mobileArray = Array.from(mobileNo);

  //checking mobileNo array
  console.log(`This is the mobileNo array ${mobileArray}`);
  console.log(mobileArray.length);

  //conditional rendering on the submit button
  useEffect(() => {
    //validation to enable the submit button when all fields are populated and both passwords match
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2 &&
      firstName !== "" &&
      lastName !== "" &&
      mobileArray.length === 11 &&
      //OR mobileNo.length
      age !== ""
    ) {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [email, password1, password2]); //optional parameter for future reference idk

  //if there is already a user will redirect to the courses page - there won't be a register page if a user is logged in
  return user.id !== null ? (
    <Redirect to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <h1>Register</h1>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter mobileNo number"
          value={mobileNo}
          onChange={(e) => setMobile(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="age">
        <Form.Label>Age</Form.Label>
        <Form.Control
          type="string"
          placeholder="Enter age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>
      {/* 
        conditionally render submit button based on isActive state 
      */}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
