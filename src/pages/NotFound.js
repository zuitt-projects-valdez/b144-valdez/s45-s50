// import React from "react";
// import { Link } from "react-router-dom";

// function NotFound() {
//   return (
//     <div>
//       <h1>Page not found</h1>
//       <p>
//         Back to <Link to="/">homepage</Link>
//       </p>
//     </div>
//   );
// }

// export default NotFound;

import Banner from "../components/Banner";

export default function Error() {
  const data = {
    title: "Page not found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back home",
  };

  return <Banner data={data} />;
}
