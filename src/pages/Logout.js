import { useContext, useEffect } from "react";
import { Redirect } from "react-router-dom";
import UserContext from "../UserContext";

export default function Logout() {
  //localStorage.clear();
  //clears the email in the local storage what redirect allows you to do

  const { unsetUser, setUser } = useContext(UserContext);

  //clear the local storage of the user's information
  unsetUser();

  useEffect(() => {
    //set the user state back to it's original value
    setUser({ id: null });
  });
  return (
    // redirects to the login page after the information is cleared from the local storage
    <Redirect to="/login" />
  );
}
