import { Fragment, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";
//import courses from "../data/courses";

export default function Courses() {
  //Checks to see if the mock data was captured
  // console.log(courses);
  // console.log(courses[0]);

  //state that will be used to store the courses retrieved from the database
  const [courses, setCourses] = useState([]); //where the courses will be stored in the database

  //useeffect to retrieve the courses from the database upon initial render of the 'courses' component
  // fetches the data then configures it into the course card
  useEffect(() => {
    fetch("http://localhost:4000/api/courses/all")
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        //sets the "courses" state to map the data retrieved from the fetch request into several "coursecard" components
        //mapping the courses array from the database then returning it in the course card format by inputting the course into the courseProp attribute - i think
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []); //empty optional parameter

  /*   const coursesData = courses.map((course) => {
    return <CourseCard key={course.id} courseProp={course} />;
  }); */
  //key attribute to identify the unique id of the document
  return <Fragment>{courses}</Fragment>;
}
