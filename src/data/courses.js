//mock database
const courseData = [
  {
    id: "wdc001",
    name: "PHP - Laravel",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam venenatis dui ut eleifend. Curabitur elit risus, tempor sodales finibus vitae, molestie id ex. Vivamus a mauris sed erat hendrerit egestas.",
    price: 45000,
    onOffer: true,
  },
  {
    id: "wdc002",
    name: "Python - Django",
    description:
      "Sudems chaka daki Cholo na ang at bakit bakit shonget ganda lang at bakit guash mahogany at nang ng sangkatuts oblation ano shogal effem ang mabaho tamalis urky mabaho ma-kyonget tanders intonses bigalou ganda lang quality control shokot na at bakit ugmas waz na sangkatuts sa at ang at katol quality control chapter kasi doonek sa at nang at nang ng pinkalou.",
    price: 50000,
    onOffer: true,
  },
  {
    id: "wdc003",
    name: "Java - Springboot",
    description:
      "Ano kabog sa ganda lang ang sa at nang bongga shala waz paminta effem na bonggakea cheapangga mabaho otoko doonek bella pinkalou mahogany ano krang-krang jowabelles chopopo na shonget fayatollah kumenis balaj jutay kasi bella 48 years antibiotic at nang na ang fayatollah kumenis nang at ang na ang wiz shala tamalis kasi tamalis tungril otoko mashumers kasi juts.",
    price: 55000,
    onOffer: true,
  },
  {
    id: "wdc004",
    name: "HTML Introduction",
    description:
      "Proin et sem posuere, suscipit odio at, dictum felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed euismod quis ante sed accumsan.",
    price: 60000,
    onOffer: true,
  },
];

export default courseData;
