// Import the Button, Row, and Col components from react-boostrap
// import Button from 'react-bootstrap/Button';
// // Bootstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

// export default function Banner() {
//   return (
//     <Row>
//       <Col className="p-5">
//         <h1>Zuitt Coding Bootcamp</h1>
//         <p>Opportunities for everyone, everywhere</p>
//         <Button variant="primary">Enroll now!</Button>
//       </Col>
//     </Row>
//   );
// }

/* 
  COMPONENTS
  - components are like JS functions
  - accept arbitrary inputs called "props" (short for properties) ({data} in the Banner component is a prop **in a {} because the data being put is an object - i think)
  - return react elements that describe what appears on the screen 
  - written like a js function 
*/

//making a reausable component
export default function Banner({ data }) {
  console.log(data);
  const { title, content, destination, label } = data;

  return (
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{content}</p>
        <Link to={destination}>{label}</Link>
      </Col>
    </Row>
  );
}
