//import state hook from react
// import { useState, useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import PropTypes from "prop-types"; //used to set props in order to view courses
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  const { _id, name, description, price } = courseProp;
  //destructure the object to use below

  //use state hook in this component to be able to store its state
  //states are used to keep track of information related to individual components
  /* 
    syntax for state hooks 
    const [destructured array getter, setter]= useState(initialGetterValue)
    getter is the received element 
    setter is how the value will be changed whether increment or decrement 

    returns an array 
  */
  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // const [isOpen, setIsOpen] = useState(false); //need to define in order to use useEffect

  // console.log(useState(0));

  // solution
  // function enroll() {
  //   setCount(count + 1);
  //   console.log("Enrollees: " + count);
  //   setSeats(seats - 1);
  //   console.log(`Seats: ${seats}`);
  // }

  /* 
    define useEffect hook to have the CourseCard component perform a certain task after every DOM update
  */
  // useEffect(() => {
  //   if (seats === 0) {
  //     setIsOpen(true); //will disable the button
  //   }
  // }, [seats]);

  return (
    <Card className="mb-2">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>

        <Link className="btn btn-primary" to={`/courses/${_id}`}>
          Details
        </Link>
      </Card.Body>
    </Card>
  );
}
