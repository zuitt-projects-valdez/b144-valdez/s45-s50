import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useHistory, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

/* 
  Hooks API 

  useEffect vs useState
  - both enhance functional components 
  - useState allows components to have states 
  - useEffects allows components to have lifecycle methods 
    - performs side effects in function components 
    - runs after the first render and after every update - renders after every change to the app - client side (maybe idk); runs after every render to the screen
    
  useContext
  - accepts a context object - which is what is returned from the react.createContext (UserContext file)
  - used to create common data that can be accessed throughout the component hierarchy without passing the props down manually to each level  
*/

export default function CourseView() {
  //obtains the user id in order to enroll the user
  const { user } = useContext(UserContext);

  //useHistory gains access of the other methods to redirect to another page - instead of using redirect
  //redirect vs history.push
  // redirect navigates a new location; overrides the current location in the history stack
  //history pushes a new entry onto the history stack
  //both are used in conditional statement but typically want to use history.push with an event like a button click that also requires some validation in its functionality
  const history = useHistory();

  //useParams hook retrieves the courseId passed via URL, allows the fetch to happen/ find the course through the id
  const { courseId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  //enroll function to enroll a user to a specific course
  //use course Id in the parameter to know which course the user is enrolling in
  const enroll = (courseId) => {
    fetch("http://localhost:4000/api/users/enroll", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // server needs to know the data type sent to it, without knowing the request cannot be processed, once the server knows what type of data it has, will then know how to parse the encoded data; application/json is the standard that the client and server understand
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        courseId: courseId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Successfully enrolled",
            icon: "success",
            text: "You have successfully enrolled for this course.",
          });
          history.push("/courses");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  // The useEffect hook is used to check if the courseId is retrieved properly
  useEffect(() => {
    console.log(courseId);

    //fetches the course details from the database and sets it into the card texts of the page - courseId variable is allowed through the useParams thing
    fetch(`http://localhost:4000/api/courses/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        //setting the different info on the card below
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [courseId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card className="mb-2">
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Class Schedule</Card.Subtitle>
              <Card.Text>8:00am - 5:00pm</Card.Text>
              {user.id !== null ? (
                <Button variant="primary" onClick={() => enroll(courseId)}>
                  Enroll
                </Button>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Login to Enroll
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
